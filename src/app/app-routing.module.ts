import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ModulesComponent } from './components/modules/modules.component';
import { AppMainComponent } from './main-components/main/app.main.component';
import { LoginComponent } from './components/login/login.component';
import { ErrorComponent } from './components/error/error.component';
import { NotfoundComponent } from './components/notfound/notfound.component';
import { AccessComponent } from './components/access/access.component';
import { AuthentificationGuard } from './guards/authentification.guard';
import { PlanningComponent } from './components/planning/planning.component';
import { EmergementComponent } from './components/emergement/emergement.component';

@NgModule({
    imports: [
        RouterModule.forRoot([
            {path: '', redirectTo: '/login', pathMatch: 'full'},
            {
                path: 'app', component: AppMainComponent,
                canActivate: [AuthentificationGuard],
                children: [
                    { path: '', component: DashboardComponent },
                    { path: 'dashboard', component: DashboardComponent },
                    { path: 'modules', component: ModulesComponent },
                    { path: 'planning', component: PlanningComponent },
                    { path: 'module/:id', component: EmergementComponent },
                ],
            },
            { path: 'login', component: LoginComponent },
            { path: 'pages/error', component: ErrorComponent },
            { path: 'pages/notfound', component: NotfoundComponent },
            { path: 'pages/access', component: AccessComponent },
            { path: '**', redirectTo: 'pages/notfound' },
        ], { scrollPositionRestoration: 'enabled', anchorScrolling: 'enabled' })
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
