import { Component, OnInit } from '@angular/core';
import { Module } from '../../interfaces/module.interface';
import { ModuleService } from '../../services/moduleservice';

@Component({
  templateUrl: './modules.component.html',
})
export class ModulesComponent implements OnInit {

  modules: Module[]

  constructor(private moduleService: ModuleService) { }

  ngOnInit(): void {
    this.moduleService.getModulesForCurrentSpeaker().then(data => this.modules = data);
  }

  onCardClick(module: Module): void {
    this.moduleService.navigateToModuleDetail(module.id);
  }
}
