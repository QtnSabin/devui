import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import SignaturePad from 'signature_pad';
import { User } from 'src/app/interfaces/user.interface';
import { ModuleService } from 'src/app/services/moduleservice';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-emergement',
  templateUrl: './emergement.component.html',
  styleUrls: ['./emergement.component.scss']
})
export class EmergementComponent implements OnInit {
  private moduleId = null;
  public module = null;
  public signingStudent: number = undefined;
  public isSignatureOpen: boolean = false;
  public isQrCodeOpen: boolean = false;
  public isInformationOpen: boolean = false;
  public selectedStudent:any = { firstname:"", lastname:"", phone:"", email:"", signature: '' };
  public isStudentsListOpen: boolean = false;
  public isCommentModalOpen: boolean = false;
  public areAllStudentsSelected: boolean = false;
  public areAllNotSignedStudentsSelected: boolean = false;
  public myAngularxQrCode: string = '';
  public notSignedStudents: any[] = [];
  public selectedStudents: any[] = [];
  public newCommentValues: any = { studentId: undefined, comment: '' };
  public students: any[] = [];

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private moduleService: ModuleService, private userService: UserService) {
    this.myAngularxQrCode = 'acodyme.com';
  }

  title = 'signatureJS';
  signaturePad?: SignaturePad;
  @ViewChild('canvas') canvasEl?: ElementRef;
  signatureImg: string = '';

  isInProgressModule() {
    const currentDateTime = new Date(Date.now())

    return new Date(this.module.start) <= currentDateTime && new Date(this.module.end) >= currentDateTime;
  }

  getModuleStartSchedule() {
    const startingDate = new Date(this.module.start);
    return this.getModuleSchedule(startingDate);
  }

  getModuleEndSchedule() {
    const endingDate = new Date(this.module.end);
    return this.getModuleSchedule(endingDate);
  }

  getModuleSchedule(date: Date) {
    return `${date.getDay()}/${date.getMonth()}/${date.getFullYear()}`;
  }

  selectUnSelectAll() {
    this.students.forEach((student) => {
      student.isSelected = this.areAllStudentsSelected;
    })

    this.updateSelectedStudentsArray();
  }

  updateSelectedStudentsArray() {
    this.selectedStudents = this.students.filter((student) => student.isSelected === true);
  }

  selectUnSelectAllNotsigned() {
    this.notSignedStudents.forEach((student) => {
      student.isSelected = this.areAllNotSignedStudentsSelected;
    })
  }

  ngAfterViewInit() {
    this.signaturePad = new SignaturePad(this.canvasEl?.nativeElement);
  }

  openSignature(studentId: number) {
    this.isSignatureOpen = true;
    this.signingStudent = studentId;
  }

  cancelSignature(studentId: number) {
    this.students.filter((student) => studentId === student.id)[0].signature = undefined;
  }

  sendSignatureEmails(students: any[]) {
    if (students.filter((student) => student.isSelected === true).length === 0) {
      return console.log('ya personne');
    }

    return console.log('ça part');
  }

  generateQrCode() {
    this.myAngularxQrCode = 'http://localhost:4200/sign';
    this.isQrCodeOpen = true;
  }

  closeModule() {
    this.notSignedStudents = this.students.filter((student) => (
      student.signature === undefined
    )).map((student) => ({ id: student.id, name: `${student.firstname} ${student.lastname}`, isSelected: false }));

    this.isStudentsListOpen = true;
  }

  handleCloseStudentsListModal(students: any[]) {
    if(students.filter((student) => student.isSelected === true).length > 0) {
      this.sendSignatureEmails(students);
      this.isStudentsListOpen = false;
      this.signingStudent = 0;
      this.isSignatureOpen = true;
    }
  }

  startDrawing(event: Event) {
    console.log(event);
    // works in device not in browser

  }

  moved(event: Event) {
    console.log(event);
    // works in device not in browser
  }

  clearPad() {
    this.signaturePad?.clear();
  }

  savePad() {
    const base64Data = this.signaturePad?.toDataURL();
    const currentStudent = this.students.filter((student) => student.id === this.signingStudent)[0];

    if (currentStudent) {
      currentStudent.signature = base64Data ?? undefined
    } else {
      this.router.navigateByUrl('/app/modules');
    }

    this.signingStudent = undefined;
    this.isSignatureOpen = false;
    this.signaturePad = new SignaturePad(this.canvasEl?.nativeElement);
  }

  getParticipants() {
    this.userService.getParticipantsFromModule(this.module).then(data => {
      const finalData = data.map((student) => ({ ...student, comment: '' }));
      this.students = finalData;
    });
  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.moduleId = params['id'];
    });

    if (this.moduleId) {
      this.moduleService.getModule(this.moduleId).then((data) => {
        this.module = data
        this.getParticipants()
      })
    }
  }

  openCommentModal(studentId: number, studentComment: string) {
    this.newCommentValues = { studentId, comment: studentComment };
    this.isCommentModalOpen = true;
  }

  writeComment() {
    this.students.filter((student) => student.id === this.newCommentValues.studentId)[0].comment = this.newCommentValues.comment;
    this.newCommentValues = { studentId: undefined, comment: '' }
    this.isCommentModalOpen = false;
  }
  handleClickStudent(student) {
    this.selectedStudent = student
    this.isInformationOpen = true
  }

  saveUpdate() {
    const student = this.students.find(std => std.id == this.selectedStudent.id)
    this.isInformationOpen = false
    console.log(this.saveUpdate)
  }
}
