import { Component, OnInit } from '@angular/core';
import { CalendarOptions } from '@fullcalendar/angular';
import { ModuleService } from '../../services/moduleservice';

@Component({
  templateUrl: './planning.component.html',
})
export class PlanningComponent implements OnInit {

  constructor(private moduleService: ModuleService) { }

  ngOnInit(): void {
    this.moduleService.getModulesForCurrentSpeaker().then(data => this.calendarOptions.events = data as any[]);
    this.calendarOptions.eventClick = (info) => {
      this.moduleService.navigateToModuleDetail(parseInt(info.event.id));
    }
  }

  calendarOptions: CalendarOptions = {
    initialView: 'timeGridWeek',
    headerToolbar: {
      left: 'prev,next today',
      center: 'title',
      right: 'dayGridMonth,timeGridWeek,timeGridDay'
    },
    locale: 'fr',
    allDaySlot: false,
    buttonText: {
      today: 'Aujourd\'hui',
      month: 'Mois',
      week: 'Semaine',
      day: 'Jour',
      list: 'list'
    },
    firstDay: 1,
    events: [],
    eventClick: function(info) {
    }
  };
}