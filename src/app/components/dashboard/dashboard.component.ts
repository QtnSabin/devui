import { Component, OnInit } from '@angular/core';

import { Module } from '../../interfaces/module.interface';
import { ModuleService } from '../../services/moduleservice';

@Component({
    templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit {

    modules: Module[];

    chartData: any;

    constructor(private moduleService: ModuleService) { }

    ngOnInit() {
        this.moduleService.getModulesForCurrentSpeaker().then(data => this.modules = data);

        this.chartData = {
            type: "",
            labels: [
                'Signés',
                'En attente de signature',
                'Absents'
            ],
            datasets: [
                {
                    label: 'My First Dataset',
                    data: [4, 2, 1],
                    backgroundColor: [
                        'rgb(212, 236, 213)',
                        'rgb(202, 230, 252)',
                        'rgb(255, 209, 206)'
                    ],
                    hoverOffset: 4
                }
            ]
        };

    }

}
