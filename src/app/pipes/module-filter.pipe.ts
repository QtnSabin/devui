import { Pipe, PipeTransform } from '@angular/core';
import { Module } from '../interfaces/module.interface';

@Pipe({
  name: 'moduleFilter'
})
export class ModuleFilterPipe implements PipeTransform {

  transform(data: Module[], moduleType: string): any {
    const currentDateTime = new Date(Date.now())

    switch(moduleType){
      case 'inProgress':
        return data.filter(module => new Date(module.start) <= currentDateTime && new Date(module.end) >= currentDateTime);
      case 'closed':
        return data.filter(module => new Date(module.end) < currentDateTime);
      case 'next':
        return data.filter(module => new Date(module.start) > currentDateTime);
      default:
        return data;
    }
  }
}
