export interface Module {
    id?: number;
    title?: string;
    start?: string;
    end?: string;
    speakers: number[],
    participants: number[],
    description?: string
    place?: string;
    closedAt?: string;
}
