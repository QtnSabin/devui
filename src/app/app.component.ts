import { Component } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent {

    menuMode = 'static';

    constructor(private primengConfig: PrimeNGConfig) { }

    ngOnInit() {
        window.addEventListener("load", () => {
            window.addEventListener("online", () => {
                console.log(navigator.onLine)
            });
            
            window.addEventListener("offline", () => {
                console.log(navigator.onLine)
            });
        });
        this.primengConfig.ripple = true;
        document.documentElement.style.fontSize = '14px';
    }
}
