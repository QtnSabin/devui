import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Module } from '../interfaces/module.interface';
import { User } from '../interfaces/user.interface';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getParticipantsFromModule(module: Module) {
    return this.http.get<any>('app/constants/dummydata/users.json')
      .toPromise()
      .then(res => res.data.filter(user => module.participants.includes(user.id)) as User[])
      .then(data => data);
  }
}
