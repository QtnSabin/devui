import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Module } from '../interfaces/module.interface';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class ModuleService {

  constructor(private http: HttpClient, private authService: AuthService, private router: Router) { }

  getModules() {
    return this.http.get<any>('app/constants/dummydata/modules.json')
      .toPromise()
      .then(res => res.data as Module[])
      .then(data => data);
  }

  navigateToModuleDetail(id: number): void {
    this.router.navigateByUrl(`/app/module/${id}`);
  }

  getModulesForCurrentSpeaker() {
    const currentUser = this.authService.getCurrentUser();
    return this.http.get<any>('app/constants/dummydata/modules.json')
      .toPromise()
      .then(res => res.data.filter(module => module.speakers.includes(currentUser.id)) as Module[])
      .then(data => data);
  }

  getModule(id: number) {
    return this.http.get<any>('app/constants/dummydata/modules.json')
      .toPromise()
      .then(res => res.data.find(module => module.id == id) as Module)
      .then(data => data);
  }
}
