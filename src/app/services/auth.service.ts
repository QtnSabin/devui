import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../interfaces/user.interface';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private router: Router, private http: HttpClient) { }

  private getUserByEmail(email) {
    return this.http.get<any>('app/constants/dummydata/users.json')
      .toPromise()
      .then(res => res.data.find(user => user.role == 'speaker' && user.email == email) as User)
      .then(data => data);
  }

  login(email: string, password: string, shouldRemember: boolean): void {
    //TODO avec le back créer un JWT pour mieux gérer la connexion user
    this.getUserByEmail(email).then((user) => {
      if (user && user.password == password) {
        user.password = null
        localStorage.setItem('userToken', JSON.stringify(user));
        this.router.navigateByUrl('/app')
      }
    })
  }

  checkIfUserExists(): boolean {
    if (localStorage.getItem('userToken') !== null && localStorage.getItem('userToken') !== undefined) {
      return true;
    }
    return false;
  }

  getCurrentUser(): User {
    const stringifyUser = localStorage.getItem('userToken')

    if (stringifyUser) return JSON.parse(stringifyUser)
    return null
  }
}
