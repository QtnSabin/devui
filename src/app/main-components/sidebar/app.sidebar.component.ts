import { Component, OnInit } from '@angular/core';
import { AppMainComponent } from '../main/app.main.component';

@Component({
    selector: 'app-sidebar',
    templateUrl: './app.sidebar.component.html'
})

export class AppSidebarComponent implements OnInit {
    menuItems: any[];

    constructor(public appMain: AppMainComponent) { }

    ngOnInit() {
        this.menuItems = [
            { label: 'Dashboard', icon: 'pi pi-fw pi-th-large', routerLink: ['/app/dashboard'] },
            { label: 'Modules', icon: 'pi pi-fw pi-book', routerLink: ['/app/modules'] },
            { label: 'Planning', icon: 'pi pi-fw pi-calendar', routerLink: ['/app/planning'] },
        ];
    }

    onKeydown(event: KeyboardEvent) {
        const nodeElement = (<HTMLDivElement>event.target);
        if (event.code === 'Enter' || event.code === 'Space') {
            nodeElement.click();
            event.preventDefault();
        }
    }
}
