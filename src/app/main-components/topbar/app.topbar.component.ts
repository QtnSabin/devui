import { Component, OnDestroy, OnInit } from '@angular/core';
import { AppMainComponent } from '../main/app.main.component';
import { MenuItem } from 'primeng/api';
import { User } from 'src/app/interfaces/user.interface';
import { AuthService } from 'src/app/services/auth.service';

@Component({
    selector: 'app-topbar',
    templateUrl: './app.topbar.component.html'
})
export class AppTopBarComponent implements OnInit {

    items: MenuItem[];

    user: User;

    constructor(public appMain: AppMainComponent, private authService: AuthService) { }

    ngOnInit(): void {
        this.user = this.authService.getCurrentUser()
    }

    goBackToLoginBtnClick() {
        localStorage.removeItem("userToken");
    }
}
